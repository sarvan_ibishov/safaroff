<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Body Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('body', 'Body:') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::text('date', null, ['class' => 'form-control','id'=>'datepicker']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Image:') !!}
    {!! Form::file('image', ['accept' => 'image/*', 'class' => 'form-control']) !!}
    @if(isset($post->id))
        {{Html::image($post->getImage($post->image),null,['class'=>'img-responsive'])}}
    @endif

</div>


<!-- User Id Field -->
{{--<div class="form-group col-sm-6">--}}
{{--{!! Form::label('user_id', 'User Id:') !!}--}}
{{--{!! Form::number('user_id', null, ['class' => 'form-control']) !!}--}}
{{--{{ Form::select('user_id',\App\Models\Users::pluck('name','id'), null, ['class' => 'form-control']) }}--}}
{{--</div>--}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('posts.index') !!}" class="btn btn-default">Cancel</a>
</div>
