<?php

namespace App\Repositories;

use App\Models\Post;
use InfyOm\Generator\Common\BaseRepository;
use Intervention\Image\Facades\Image;

/**
 * Class PostRepository
 * @package App\Repositories
 * @version September 7, 2017, 2:19 pm UTC
 *
 * @method Post findWithoutFail($id, $columns = ['*'])
 * @method Post find($id, $columns = ['*'])
 * @method Post first($columns = ['*'])
 */
class PostRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'body',
        'slug',
        'date',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Post::class;
    }

    /**
     * upload image and return name
     */
    public function setAndGetImage($request)
    {
        $imageName = time() . '.' . $request->image->getClientOriginalExtension();
       // $request->image->move(public_path('images'), $imageName);
        Image::make($request->file('image'))->fit(1900, 600)->save(public_path('images').'/'. $imageName);
        return $imageName;

    }
}
