<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App\Models
 * @version September 7, 2017, 2:19 pm UTC
 *
 * @property string title
 * @property string description
 * @property string body
 * @property string slug
 * @property string|\Carbon\Carbon date
 * @property integer user_id
 */
class Post extends Model
{
    /// use SoftDeletes;

    public $table = 'posts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['date'];


    public $fillable = [
        'title',
        'description',
        'body',
        'slug',
        'date',
        'user_id',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'body' => 'string',
        'slug' => 'string',
        'image' => 'string',
        'user_id' => 'integer'
    ];


    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|max:500',
        'description' => 'max:500',
        'body' => 'required',
        'slug' => 'required|max:500',
        'date' => 'required|date',
        'user_id' => 'required|integer',
        'image' => 'image|mimes:jpeg,png,jpg,gif,svg',


    ];

    public function getImage($name)
    {

        return asset("images/" . $name);
    }


}
