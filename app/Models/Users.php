<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Users
 * @package App\Models
 * @version September 8, 2017, 10:44 am UTC
 *
 */
class Users extends Model
{


    public $table = 'users';


    public $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|max:255',
        'email' => 'required|email|unique:users|max:100',
        'password'=>'required'

    ];


}
