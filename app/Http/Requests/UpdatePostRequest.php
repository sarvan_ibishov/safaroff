<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Post;

class UpdatePostRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Post::$rules;
    }

    protected function getValidatorInstance()
    {
        $input = $this->all();
        $input['user_id'] = \Auth::user()->id;
        if (empty($input['slug']))
            $input['slug'] = str_slug($input['title']);
        $this->getInputSource()->replace($input);

        /*modify data before send to validator*/

        return parent::getValidatorInstance();
    }
}
